/// <reference path="typings/angular2/angular2.d.ts" />

import { Component, View, bootstrap, For} from 'angular2/angular2';


@Component({
  selector: 'my-app'
})
@View({
  templateUrl:'nameView.html'
})
class MyAppComponent {
  name: string;

  constructor() {
    this.name = 'Alice';
  }
}

bootstrap(MyAppComponent);
