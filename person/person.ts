import { Component, View, bootstrap, For, If} from "angular2/angular2";

@Component({ selector: "display" })
@View({ templateUrl: "personView.html", directives: [If, For] })
class PersonComponent {
  persons: Array<string>;

  constructor() {
    this.persons = ["Andreas", "Thomas", "Christof", "Steffen"];
  }

  newPersonEvent($event, newPerson) {
    if ($event.which === 13 && $event.target.value !== "") {
      this.addPerson($event.target.value);
      $event.target.value = null;
    }
  }

  addPerson(person: string) {
    this.persons.push(person);
  }

  deletePerson(person: string) {
    var pos: number = this.persons.indexOf(person);
    if (pos >= 0)
      this.persons.splice(pos, 1);
  }
}

bootstrap(PersonComponent);
