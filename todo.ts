/// <reference path="typings/angular2/angular2.d.ts" />

import { Component, View, bootstrap, For} from 'angular2/angular2';


@Component({
  selector: 'todo-app'
})
@View({
  templateUrl: 'todoView.html', directives: [For]
})
class MyAppComponent {
  tasks: Array<Task>;

  constructor() {
    this.tasks = new Array<Task>();
  }

  enterTask($event) {
    if ($event.which === 13) {
      this.addTask($event.target.value);
      $event.target.value = '';
    }
  }

  addTask(newTask) {
    this.tasks.push(new Task(newTask));
  }

  toggleState(task: Task) {
    task.isDone = !task.isDone;
  }

  allDone() {
    for (let i = 0; i < this.tasks.length; i++){
      this.tasks[i].isDone = true;
    }
  }

  removeDone() {
    var tempTasks = new Array<Task>();
    for (let i = 0; i < this.tasks.length; i++){
      if (!this.tasks[i].isDone)
        tempTasks.push(this.tasks[i]);
    }
    this.tasks = tempTasks;
  }
}

bootstrap(MyAppComponent);


//class TodoComponent{
// tasks : Array<Task>;

// addTask(newTask){
//   this.tasks.push(new Task(newTask));
// }
//
// completeTask(task){
//   task.isDone = !task.isDone;
// }
//
// cleanTasks(){
//   tempTaskArray : Array<Task>();
//
//   for (let i = 0; i < this.tasks.length; i++) {
//       if (this.tasks[i].isDone){
//         this.tasks.slice(i, 1);
//       }
//   }
// }
//}

class Task {
  name: string;
  isDone: boolean;

  constructor(name) {
    this.name = name;
    this.isDone = false;
  }
}
