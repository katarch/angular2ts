/// <reference path="typings/angular2/angular2.d.ts" />
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var angular2_1 = require('angular2/angular2');
var MyAppComponent = (function () {
    function MyAppComponent() {
        this.tasks = new Array();
    }
    MyAppComponent.prototype.enterTask = function ($event) {
        if ($event.which === 13) {
            this.addTask($event.target.value);
            $event.target.value = '';
        }
    };
    MyAppComponent.prototype.addTask = function (newTask) {
        this.tasks.push(new Task(newTask));
    };
    MyAppComponent.prototype.toggleState = function (task) {
        task.isDone = !task.isDone;
    };
    MyAppComponent.prototype.allDone = function () {
        for (var i = 0; i < this.tasks.length; i++) {
            this.tasks[i].isDone = true;
        }
    };
    MyAppComponent.prototype.removeDone = function () {
        var tempTasks = new Array();
        for (var i = 0; i < this.tasks.length; i++) {
            if (!this.tasks[i].isDone)
                tempTasks.push(this.tasks[i]);
        }
        this.tasks = tempTasks;
    };
    MyAppComponent = __decorate([
        angular2_1.Component({
            selector: 'todo-app'
        }),
        angular2_1.View({
            templateUrl: 'todoView.html', directives: [angular2_1.For]
        })
    ], MyAppComponent);
    return MyAppComponent;
})();
angular2_1.bootstrap(MyAppComponent);
var Task = (function () {
    function Task(name) {
        this.name = name;
        this.isDone = false;
    }
    return Task;
})();
